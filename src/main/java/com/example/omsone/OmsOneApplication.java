package com.example.omsone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OmsOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(OmsOneApplication.class, args);
	}

}
